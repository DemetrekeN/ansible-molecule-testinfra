"""testinfra for HOST"""

import pytest

def test_pkg_exists(host):
    """Checking pkg exists."""
    assert host.package("nginx").is_installed

def test_nginx_running(host):
    """Checking is nginx running."""
    assert host.service("nginx").is_running

def test_directory_exists(host):
    """Checking directory exists."""
    assert host.file("/home/genadiy/project-portfolio").is_directory

def test_virtual_host_exists(host):
    """Checking virtual host exists."""
    assert host.file("/etc/nginx/sites-available/torpeda").exists

def test_symlink_exists(host):
    """Checking symlink host exists."""
    assert host.file("/etc/nginx/sites-enabled/torpeda").is_symlink

def test_config_exists(host):
    """Checking custom configs exists."""
    assert host.file("/etc/nginx/conf.d/logs.conf").exists
    assert host.file("/etc/nginx/nginx.conf").exists
